import { Typography } from '@mui/material';
import { useTypeSafeTranslation } from 'hooks/useTypeSafeTranslation';
import MainLayout from 'layout/MainLayout';
import type { PageComponent } from 'next';

export const ExamplePage: PageComponent = () => {
  const { t } = useTypeSafeTranslation();
  return <Typography variant="h1">{t('common.helloWorld')}</Typography>;
};

ExamplePage.getLayout = function getLayout(page) {
  return <MainLayout>{page}</MainLayout>;
};
