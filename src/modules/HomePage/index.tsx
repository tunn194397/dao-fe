import { Typography } from '@mui/material';
import { useTypeSafeTranslation } from 'hooks/useTypeSafeTranslation';
import MainLayout from 'layout/MainLayout';
import type { PageComponent } from 'next';

export const HomePage: PageComponent = () => {
  const { t } = useTypeSafeTranslation();
  return <Typography variant="body1" color="text.primary">{t('common.helloWorld')}</Typography>;
};

HomePage.getLayout = function getLayout(page) {
  return <MainLayout>{page}</MainLayout>;
};
