import { Paths } from '@/types/util.types';
import { useTranslation } from 'next-i18next';

import translations from '../../public/locales/en/common.json';

export type TranslationKeys = Paths<typeof translations>;
export type TFunctionType = (s: TranslationKeys, p?: any) => string;

export const useTypeSafeTranslation = () => {
  const { t, ...rest } = useTranslation();

  return {
    t: (s: TranslationKeys, p?: any) => t(s, p),
    ...rest,
  };
};
