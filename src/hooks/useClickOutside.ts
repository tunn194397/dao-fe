import { useCallback, useEffect, useMemo, useRef } from 'react';

export const useClickOutside = <E extends HTMLElement>(cb?: (e: MouseEvent) => void) => {
  const ref = useRef<E | null>(null);

  useEffect(() => {
    const { current } = ref;
    if (!cb) return;

    if (!current) return;

    const onClick = (e: MouseEvent) => {
      if (!current) return;

      if (current === e.target || current.contains(e.target as Element)) return;
      cb(e);
    };

    window.addEventListener('click', onClick);

    return () => {
      window.removeEventListener('click', onClick);
    };
  }, [cb]);

  const setRef = useCallback((el: E | null) => (ref.current = el), []);

  return {
    ref,
    setRef,
  };
};
