import { useSocket } from 'socket.io-react-hook';
import { SOCKET_URL } from 'utils/constants';

export const useAuthWebSocket = () => {
  const enabled = false;
  const sk = useSocket(SOCKET_URL, {
    enabled: enabled,
    query: {},
    transports: ['websocket'],
    reconnection: true,
    reconnectionDelay: 1000,
    reconnectionDelayMax: 5000,
    reconnectionAttempts: 4,
  });

  return { ...sk };
};
