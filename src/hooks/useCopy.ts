import { useState } from 'react';

type CopyFn = (text: string) => void; // Return success

export function useCopy(): [boolean, CopyFn] {
  const [copied, setCopied] = useState(false);

  const copy = async (text: string) => {
    if (!navigator?.clipboard) {
      return console.log('Not support copy');
    }

    // Try to save to clipboard then save it in the state if worked
    try {
      await navigator.clipboard.writeText(text);
      setCopied(true);
      setTimeout(() => {
        setCopied(false);
      }, 2000);
    } catch (error) {
      console.warn('Copy failed', error);
      setCopied(false);
    }
  };

  return [copied, copy];
}
