import { combineReducers } from '@reduxjs/toolkit';
import example from './example/slice';

const createRootReducer = () => {
  return combineReducers({
    example,
  });
};

export default createRootReducer;
