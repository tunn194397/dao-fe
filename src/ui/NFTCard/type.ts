export interface INFTProps {
  url: string;
  categoryName: string;
  nftName: string;
  price: string;
  suffix: 'USDC';
}

export interface INFTCardProps {
  item: INFTProps;
}
