import { FC } from 'react';
import SomeThing from './SomeThing';

const Example: FC = () => {
  return (
    <>
      <SomeThing />
    </>
  );
};

export default Example;
