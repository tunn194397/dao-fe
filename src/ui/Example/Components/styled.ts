import { Box } from '@mui/material';
import { styled } from '@mui/material/styles';

// Just example
export const ExampleStyled = styled(Box)(() => ({
  zIndex: 1,
}));
