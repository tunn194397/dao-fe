import { SystemThemeOptions } from '@mui/system';
declare module '@mui/material/styles' {
  interface CustomPalette {
    primaryDark1?: PaletteColorOptions;
    primaryDark2?: PaletteColorOptions;
    primaryDark3?: PaletteColorOptions;
    secondaryDark1?: PaletteColorOptions;
    secondaryDark2?: PaletteColorOptions;
    secondaryDark3?: PaletteColorOptions;
    primaryLight1?: PaletteColorOptions;
    primaryLight2?: PaletteColorOptions;
    primaryLight3?: PaletteColorOptions;
    secondaryLight1?: PaletteColorOptions;
    secondaryLight2?: PaletteColorOptions;
    secondaryLight3?: PaletteColorOptions;
  }
  interface Palette extends CustomPalette {}
  interface PaletteOptions extends CustomPalette {}
}

declare module '@mui/material/Button' {
  interface ButtonPropsColorOverrides {
    primaryDark1?: true;
    primaryDark2?: true;
    primaryDark3?: true;
    secondaryDark1?: true;
    secondaryDark2?: true;
    secondaryDark3?: true;
    primaryLight1?: true;
    primaryLight2?: true;
    primaryLight3?: true;
    secondaryLight1?: true;
    secondaryLight2?: true;
    secondaryLight3?: true;
  }
}
