import { COOKIES, getCookies, removeCookies } from 'lib/cookies';
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { API_URL, isServer } from 'utils/constants';

export const request = axios.create({
  baseURL: API_URL,
});

const handleError = (err: AxiosError) => {
  const data = err?.response;

  // Logout
  if (data?.status === 401) {
    removeCookies(COOKIES.token);
    removeCookies(COOKIES.user);
    if (!isServer) {
      location.href = '/';
    }
  }
  return Promise.reject(data);
};

const handleSuccess = (res: AxiosResponse) => {
  return res.data;
};

request.interceptors.response.use(handleSuccess, handleError);

request.interceptors.request.use(
  async (config: AxiosRequestConfig) => {
    const token = getCookies(COOKIES.token);

    if (token) {
      config = {
        ...config,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
    }

    return config;
  },
  (error) => Promise.reject(error)
);
