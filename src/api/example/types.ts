export interface IExampleTypeRequest {}
export interface IExampleTypeResponse {
  data: IExampleChildResponse[];
}

export interface IExampleChildResponse {}
