export const __prod__ = process.env.NODE_ENV === 'production';

export const LOCAL_STORAGE = {
  token: 'token',
};

export const API_URL = process.env.API_URL;

export const isServer = typeof window === 'undefined';

export const SOCKET_URL = 'SOCKET_URL';

