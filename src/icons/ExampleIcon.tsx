import * as React from 'react';

export const ExampleIcon = (props: React.SVGProps<SVGSVGElement>) => (
  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <rect x="5" width="24" height="2" rx="1" transform="rotate(90 5 0)" fill="#D0D0DC" />
    <rect x="13" width="24" height="2" rx="1" transform="rotate(90 13 0)" fill="#D0D0DC" />
    <rect x="21" width="24" height="2" rx="1" transform="rotate(90 21 0)" fill="#D0D0DC" />
    <rect x="6" y="3" width="6" height="4" rx="2" transform="rotate(90 6 3)" fill="#D0D0DC" />
    <rect x="14" y="14" width="6" height="4" rx="2" transform="rotate(90 14 14)" fill="#D0D0DC" />
    <rect x="22" y="5" width="6" height="4" rx="2" transform="rotate(90 22 5)" fill="#D0D0DC" />
  </svg>
);
