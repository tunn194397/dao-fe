import createEmotionCache from '@/styles/createEmotionCache';
import 'styles/nprogress.css';
import '@/styles/globals.css';

import { CacheProvider, EmotionCache } from '@emotion/react';
import { CssBaseline } from '@mui/material';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import { ReactElement, ReactNode, useState } from 'react';
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query';
import { appWithTranslation } from 'next-i18next';
import { Provider } from 'react-redux';
import store from '@/store/index';
import { NextPage } from 'next';
import Web3Provider from '@/lib/web3/Web3Provider';
import { IoProvider } from 'socket.io-react-hook';
import CustomThemeProvider from '@/styles/theme/Provider';

export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
  getLayout?: (page: ReactElement) => ReactNode;
};

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

const queryClientOption = {
  defaultOptions: {
    queries: { refetchOnWindowFocus: false, retry: false, staleTime: 1000 * 5 },
  },
};

const App = (props: MyAppProps) => {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
  const [queryClient] = useState(() => new QueryClient(queryClientOption));
  const getLayout = Component.getLayout || ((page) => page);

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>

      <Provider store={store}>
        <CustomThemeProvider>
          <IoProvider>
            <QueryClientProvider client={queryClient}>
              <Hydrate state={pageProps.dehydratedState}>
                <Web3Provider>
                  {getLayout(<Component {...pageProps} />)}
                </Web3Provider>
              </Hydrate>
            </QueryClientProvider>
          </IoProvider>
        </CustomThemeProvider>
      </Provider>
    </CacheProvider>
  );
};

export default appWithTranslation(App);
