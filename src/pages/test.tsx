import { Button, Grid, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { NFTCard } from '../ui';
import { INFTProps } from '@/ui/NFTCard/type';

const DATA_NFT: INFTProps[] = [{ url: '1', categoryName: '2', nftName: '3', price: '4', suffix: 'USDC' }];

const Test = () => {
  return (
    <Grid spacing={4} container>
      <Grid item>
        <Button variant="contained" color="primary">
          Contained
        </Button>
      </Grid>
      <Grid item>
        <Typography variant="h1">H1</Typography>
        <Typography variant="h2">H2</Typography>
      </Grid>
      <Grid item>
        <Button variant="outlined" color="primary">
          Contained
        </Button>
      </Grid>
      <Grid item>
        <Grid container spacing={4}>
          {[...DATA_NFT, ...DATA_NFT, ...DATA_NFT].map((item) => (
            <Grid item xs={3} key={item.url}>
              <NFTCard item={item} />
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Test;
