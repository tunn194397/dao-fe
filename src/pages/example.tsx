// With Static page

// import { ExamplePage } from '@/modules/ExamplePage';
// import { GetStaticPropsContext } from 'next';
// import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

// export default ExamplePage;

// export const getStaticProps = async (ctx: GetStaticPropsContext) => {
//   console.log(ctx.locale);
//   return {
//     props: {
//       ...(await serverSideTranslations(ctx.locale as string)),
//     },
//   };
// };

// With server side page
import { isAuthenticated } from '@/lib/isAuthenticated';
import { ExamplePage } from '@/modules/ExamplePage';
import { GetServerSidePropsContext } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

export default ExamplePage;

export async function getServerSideProps(ctx: GetServerSidePropsContext) {
  const { isLoggedIn } = isAuthenticated(ctx);

  if (!isLoggedIn) {
    return {
      redirect: { permanent: true, destination: '/?form=login' },
    };
  }

  return {
    props: {
      ...(await serverSideTranslations(ctx.locale as string)),
    },
  };
}
