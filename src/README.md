[Follow the example folder in every folder to make convention]()
# UI Folder
Common components for website
# Icons Folder
Save icons file tsx

# API Folder
Handle API

# Contracts Folder
Save Json and build file json contract

# Hooks Folder
create hooks to handle for website

# Layout Folder
Layout of website

# Lib Folder
Handle or cusom logic of library

# Module folder
Pages of website

# Pages folder
Pages folder of next js (read next js documents to undersand more)

# Store Folder
Redux

# Style folder
Styles and theme

# Types folder
custom type of folder

# utils folder
Utils folder like constants or something define to handle

-------------------------------------------------------------------------------

# For text: use Typography MUI. Restrict the use of HTML  tags(Priority is MUI)
# Use Common base in UI
# Define Color before code. Or use color in theme(styles folder)
# Format code use prettier



-------------------------------------------------------------------------------

# Git flow:
Create new Brand from develop
- commit with message follow: 
    With Task
    - git commit -m "Name-NameOfTask"
    With Bug
    - git commit -m "Name-FixBug-DetailOfBug"

- git push origin YOUR_BRAND
- Create new pull request
- Assign Khiemldk


-------------------------------------------------------------------------------

