import { GetServerSidePropsContext } from 'next';
import { parseCookies } from 'nookies';
import { parseJson } from 'utils/common';
import { COOKIES } from './cookies';

export function isAuthenticated(ctx: GetServerSidePropsContext) {
  const cookies = parseCookies(ctx);
  const token = cookies[COOKIES.token];

  if (!token) {
    return {
      isLoggedIn: false,
      user: undefined,
    };
  }
  return {
    isLoggedIn: true,
    user: parseJson(cookies[COOKIES.user]) as any,
  };
}
