import { ReactFCWithChildren } from '@/types/react';
import { getDefaultProvider } from 'ethers';
import { createClient, WagmiConfig } from 'wagmi';

const client = createClient({
  autoConnect: true,
  provider: getDefaultProvider(),
});

const Web3Provider: ReactFCWithChildren = ({ children }) => {
  return <WagmiConfig client={client}>{children}</WagmiConfig>;
};

export default Web3Provider;
