export type Chain = {
  chainId: number;
  name: string;
  nativeToken: string;
  getExplorerAddressLink: (address: string) => string;
  getExplorerTransactionLink: (address: string) => string;
  iconUrl: string;
  getTokenURL: (address: string, token: any) => string;
};

const etherscanUrl = 'https://etherscan.io';
const ropstenEtherscanUrl = 'https://ropsten.etherscan.io';
const rinkebyEtherscanUrl = 'https://rinkeby.etherscan.io';
const goerliEtherscanUrl = 'https://goerli.etherscan.io';
const kovanEtherscanUrl = 'https://kovan.etherscan.io';
const polygonScanUrl = 'https://polygonscan.com';
const mumbaiScanUrl = 'https://mumbai.polygonscan.com';
const bscScanUrl = 'https://bscscan.com';
const bscTestScanUrl = 'https://testnet.bscscan.com';

export const CHAIN: Record<number, Chain> = {
  1: {
    name: 'Ethereum',
    nativeToken: 'ETH',
    chainId: 1,
    getExplorerAddressLink: (address: string) => `${etherscanUrl}/address/${address}`,
    getExplorerTransactionLink: (transactionHash: string) => `${etherscanUrl}/tx/${transactionHash}`,
    iconUrl: '/images/wallet/eth.svg',
    getTokenURL: (address: string, token: any) => `${etherscanUrl}/token/${address}?a=${token}`,
  },
  3: {
    name: 'Ropsten',
    nativeToken: 'ETH',
    chainId: 3,
    getExplorerAddressLink: (address: string) => `${ropstenEtherscanUrl}/address/${address}`,
    getExplorerTransactionLink: (transactionHash: string) => `${ropstenEtherscanUrl}/tx/${transactionHash}`,
    iconUrl: '/images/wallet/eth.svg',
    getTokenURL: (address: string, token: any) => `${ropstenEtherscanUrl}/token/${address}?a=${token}`,
  },
  4: {
    name: 'Rinkeby',
    nativeToken: 'ETH',
    chainId: 4,
    getExplorerAddressLink: (address: string) => `${rinkebyEtherscanUrl}/address/${address}`,
    getExplorerTransactionLink: (transactionHash: string) => `${rinkebyEtherscanUrl}/tx/${transactionHash}`,
    iconUrl: '/images/wallet/eth.svg',
    getTokenURL: (address: string, token: any) => `${rinkebyEtherscanUrl}/token/${address}?a=${token}`,
  },
  5: {
    name: 'Goerli',
    nativeToken: 'ETH',
    chainId: 5,
    getExplorerAddressLink: (address: string) => `${goerliEtherscanUrl}/address/${address}`,
    getExplorerTransactionLink: (transactionHash: string) => `${goerliEtherscanUrl}/tx/${transactionHash}`,
    iconUrl: '/images/wallet/eth.svg',
    getTokenURL: (address: string, token: any) => `${goerliEtherscanUrl}/token/${address}?a=${token}`,
  },
  42: {
    name: 'Kovan',
    nativeToken: 'ETH',
    chainId: 42,
    getExplorerAddressLink: (address: string) => `${kovanEtherscanUrl}/address/${address}`,
    getExplorerTransactionLink: (transactionHash: string) => `${kovanEtherscanUrl}/tx/${transactionHash}`,
    iconUrl: '/images/wallet/eth.svg',
    getTokenURL: (address: string, token: any) => `${kovanEtherscanUrl}/token/${address}?a=${token}`,
  },
  56: {
    name: 'Binance smart chain',
    nativeToken: 'BSC',
    chainId: 56,
    getExplorerAddressLink: (address: string) => `${bscScanUrl}/address/${address}`,
    getExplorerTransactionLink: (transactionHash: string) => `${bscScanUrl}/tx/${transactionHash}`,
    iconUrl: '/images/wallet/bsc.svg',
    getTokenURL: (address: string, token: any) => `${bscScanUrl}/token/${address}?a=${token}`,
  },
  97: {
    name: 'Smart Chain - Testnet',
    nativeToken: 'BSC',
    chainId: 97,
    getExplorerAddressLink: (address: string) => `${bscTestScanUrl}/address/${address}`,
    getExplorerTransactionLink: (transactionHash: string) => `${bscTestScanUrl}/tx/${transactionHash}`,
    iconUrl: '/images/wallet/bsc.svg',
    getTokenURL: (address: string, token: any) => `${bscTestScanUrl}/token/${address}?a=${token}`,
  },
  137: {
    name: 'Polygon',
    nativeToken: 'MATIC',
    chainId: 137,
    getExplorerAddressLink: (address: string) => `${polygonScanUrl}/address/${address}`,
    getExplorerTransactionLink: (transactionHash: string) => `${polygonScanUrl}/tx/${transactionHash}`,
    iconUrl: '/images/wallet/polygon.svg',
    getTokenURL: (address: string, token: any) => `${polygonScanUrl}/token/${address}?a=${token}`,
  },
  80001: {
    name: 'Mumbai',
    nativeToken: 'MATIC',
    chainId: 80001,
    getExplorerAddressLink: (address: string) => `${mumbaiScanUrl}/address/${address}`,
    getExplorerTransactionLink: (transactionHash: string) => `${mumbaiScanUrl}/tx/${transactionHash}`,
    iconUrl: '/images/wallet/polygon.svg',
    getTokenURL: (address: string, token: any) => `${mumbaiScanUrl}/token/${address}?a=${token}`,
  },
};
