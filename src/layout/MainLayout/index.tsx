import { ReactFCWithChildren } from '@/types/react';
import React from 'react';
import Footer from './Footer';
import Navbar from './Navbar';
import { Content } from './Navbar/styled';

const MainLayout: ReactFCWithChildren = ({ children }) => {
  return (
    <>
      <Navbar />
      <Content>{children}</Content>
      <Footer />
    </>
  );
};

export default MainLayout;
