import { styled } from '@mui/material/styles';

export const Content = styled('main')(({ theme }) => ({
  minHeight: 'calc(100vh - 80px)',
  paddingBottom: '160px',
  width: '100%',
}));
