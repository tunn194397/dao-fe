import { PaletteMode } from '@mui/material';
import { ThemeOptions } from '@mui/material/styles';

/* Override ⚛️ palette*/
export const getPaletteStyles = (mode: PaletteMode): ThemeOptions['palette'] => ({
  mode,
  primaryDark1: {
    main: '#537874',
    contrastText: '#FFFFFF',
  },
  primaryDark2: {
    main: '#39524F',
    contrastText: '#FFFFFF',
  },
  primaryDark3: {
    main: '#253634',
    contrastText: '#FFFFFF',
  },
  secondaryDark1: {
    main: '#938257',
    contrastText: '#FFFFFF',
  },
  secondaryDark2: {
    main: '#78632D',
    contrastText: '#FFFFFF',
  },
  secondaryDark3: {
    main: '#4F411E',
    contrastText: '#FFFFFF',
  },
  primaryLight1: {
    main: '#F5F8F3',
    contrastText: '#FFFFFF',
  },
  primaryLight2: {
    main: '#EBF1E7',
    contrastText: '#FFFFFF',
  },
  primaryLight3: {
    main: '#CDDCC4',
    contrastText: '#FFFFFF',
  },
  secondaryLight1: {
    main: '#F5F2E8',
    contrastText: '#FFFFFF',
  },
  secondaryLight2: {
    main: '#EBE6D1',
    contrastText: '#FFFFFF',
  },
  secondaryLight3: {
    main: '#E1DCC8',
    contrastText: '#FFFFFF',
  },
  primary: {
    main: '#39524F',
    contrastText: '#ffffff',
  },
  secondary: {
    main: '#F5F2E8',
    contrastText: '#78632D',
  },
  text: {
    primary: '#39524F',
    secondary: '#78632D',
  },
  error: {
    main: '#B16F5C',
    contrastText: '#FFFFFF',
  },
  warning: {
    main: '#D6B557',
    contrastText: '#FFFFFF',
  },
  success: {
    main: '#6FA176',
    contrastText: '#FFFFFF',
  },
  background: {
    default: mode === 'dark' ? '#212122' : '#FFFFFF',
    paper: mode === 'dark' ? '#212122' : '#FFFFFF',
  },
});
