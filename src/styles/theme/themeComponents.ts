import { PaletteMode } from '@mui/material';
import { ThemeOptions } from '@mui/material/styles';

/* Override ⚛️ components*/
export const getComponentStyles = (mode: PaletteMode): ThemeOptions['components'] => ({
  MuiButton: {
    styleOverrides: {
      root: {
        '&:hover': {
          boxShadow: 'none',
          // backgroundColor: 'initial',
        },
      },
      contained: {
        boxShadow: 'none',
        border: '1px solid #78632D',

        '&.Mui-disabled': {},
      },
      outlined: {
        '&.Mui-disabled': {},
        '&.Mui-disabled *': {},
      },
      outlinedSecondary: {},
    },
  },
  MuiCard: {
    styleOverrides: {},
  },
});
